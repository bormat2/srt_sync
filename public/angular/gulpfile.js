const { watch, src, dest, task } = require('gulp')
const rename = require("gulp-rename")
const run = require('gulp-run')
const base = './dist/'
const source = base + 'index.html'
const destination = 'index.ejs'
const fs = require('fs')

// must start and end with '/'
const root_path = '/random_path/' // if we want index at http://domain.com/random_path/index.html

// create variable containing the root path for javascript and sass
function gulp_path(prod_or_dev){
    let root_path_2 = prod_or_dev === 'prod' ? root_path : '/'
    console.log('path will be ' +  root_path_2)
    const writeFilePromise = function(file, content){
        return new Promise((resolve) => {
            fs.writeFile(file, content,(...args)=>{
                resolve(...args)
            })
        })
    }
    return Promise.all([
        writeFilePromise('src/gulp_info.js', `

            var root_path = '${root_path_2}'
            // usefull to have a slash as last and first character
            root_path = ('/'+ root_path + '/').replace(/\\/+/g,'/')

            if(typeof module !== "undefined"){
                module.exports = {root_path: root_path}
            }else{
                window.root_path = root_path
            }
        `),
        writeFilePromise('src/gulp_info.scss', `
            $root_path : '${root_path_2}';
        `)
    ])
}

function build(prod_or_dev){
    // change_index_extension()
    const p1 = gulp_path(prod_or_dev)
    const p2 = new Promise(function(resolve, reject) {
        if(prod_or_dev === 'prod'){
            console.log('it is normal if it takes 1 minute to compile')
            run('ng build --prod').exec(function(){
                src(source)
                    .pipe(rename('index.ejs'))
                    .pipe(dest(base))
                resolve()
            })
        }else{
            console.log('dev mode')
            run('ng serve --port 5002 --proxy-config proxy-conf.json').exec(function(){
                resolve()
            })
        }
    })
    return Promise.all([p1, p2])
}

// build the project to be launch from root_path
task('build_prod', () => build('prod'))

// launch the dev mode
task('dev_mode', () => build('dev'))

// exports.default = build
