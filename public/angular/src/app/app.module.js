"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const utils_ang_1 = require("./utils_ang");
const ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
const platform_browser_1 = require("@angular/platform-browser");
const core_1 = require("@angular/core");
const router_1 = require("@angular/router");
const forms_1 = require("@angular/forms");
const angular_file_1 = require("angular-file");
const app_component_1 = require("./app.component");
const animations_1 = require("@angular/platform-browser/animations");
const comp1_component_1 = require("./comp1/comp1.component");
const comp2_component_1 = require("./comp2/comp2.component");
const auth_component_1 = require("./auth/auth.component");
const http_1 = require("@angular/common/http");
const forms_2 = require("@angular/forms");
const scripts_1 = require("./scripts");
const material_1 = require("@angular/material");
const my_let_directive_1 = require("./my-let.directive");
const srt_time_input_component_1 = require("./srt-time-input/srt-time-input.component");
const appRoutes = [
    {
        path: utils_ang_1.prefix_root + 'app default',
        component: app_component_1.AppComponent,
        data: { title: 'app' }
    },
    {
        path: utils_ang_1.prefix_root + 'comp1/:id',
        component: comp1_component_1.Comp1Component,
        data: { title: 'comp1' }
    },
    {
        path: utils_ang_1.prefix_root + 'comp2',
        component: comp2_component_1.Comp2Component,
        data: { title: 'comp2' }
    },
    {
        path: utils_ang_1.prefix_root + 'form',
        component: scripts_1.TemplateFormComponent,
        data: { title: 'form' }
    },
    {
        path: utils_ang_1.prefix_root + 'auth',
        component: auth_component_1.AuthComponent,
        data: { title: 'comp2' }
    },
];
let AppModule = class AppModule {
};
AppModule = __decorate([
    core_1.NgModule({
        declarations: [
            app_component_1.AppComponent,
            comp1_component_1.Comp1Component,
            comp2_component_1.Comp2Component,
            auth_component_1.AuthComponent,
            scripts_1.TemplateFormComponent,
            my_let_directive_1.LetDirective,
            srt_time_input_component_1.SrtTimeInputComponent,
        ],
        imports: [
            ng_bootstrap_1.NgbModule,
            http_1.HttpClientModule,
            angular_file_1.ngfModule,
            forms_1.ReactiveFormsModule,
            animations_1.BrowserAnimationsModule,
            material_1.MatButtonModule,
            material_1.MatCardModule,
            material_1.MatInputModule,
            material_1.MatToolbarModule,
            forms_2.FormsModule,
            router_1.RouterModule.forRoot(appRoutes, { enableTracing: false }),
            platform_browser_1.BrowserModule,
        ],
        providers: [http_1.HttpClientModule],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
