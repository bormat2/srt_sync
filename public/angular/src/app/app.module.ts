import {prefix_root} from './utils_ang';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
// ...other imports
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

const config: SocketIoConfig = { url: '/', options: {} };
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import { ngfModule } from "angular-file"
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { Comp1Component } from './comp1/comp1.component';
import { Comp2Component } from './comp2/comp2.component';
import { AuthComponent } from './auth/auth.component';
import { HttpClientModule } from '@angular/common/http';
import {
    FormsModule,
} from '@angular/forms';
import {TemplateFormComponent} from './templateForm/templateForm.component'
import {
  MatButtonModule,
  MatCardModule,
  MatToolbarModule,
  MatInputModule,
  MatSidenavModule,
  // MatAutocompleteModule,
  // MatButtonToggleModule,
  // MatCheckboxModule,
  // MatChipsModule,
  // MatDatepickerModule,
  // MatDialogModule,
  // MatExpansionModule,
  // MatGridListModule,
  MatIconModule,
  MatListModule,
  // MatMenuModule,
  // MatPaginatorModule,
  // MatProgressBarModule,
  // MatProgressSpinnerModule,
  // MatRadioModule,
  MatSelectModule,
  // MatSidenavModule,
  // MatSliderModule,
  // MatSlideToggleModule,
  // MatSnackBarModule,
  // MatTableModule,
  MatTabsModule,
  // MatTooltipModule
} from 
   '@angular/material';
import { LetDirective } from './my-let.directive';
import { SrtTimeInputComponent } from './srt-time-input/srt-time-input.component';
import { HeaderComponent } from './navigation/header/header.component';
// import {JokeListComponent, JokeFormComponent, JokeComponent} from './scripts'
// import { AuthService } from './Services/auth.service';

const appRoutes: Routes = [
  // { path: 'hero/:id',      component: HeroDetailComponent },
  {
    path: prefix_root + 'app default',
    component: AppComponent,
    data: { title: 'app' }
  },
  // {
  //   path: prefix_root + 'comp1/:id',
  //   component: Comp1Component,
  //   data: { title: 'comp1' }
  // },
  {
    path: prefix_root + 'comp2',
    component: Comp2Component,
    data: { title: 'comp2' }
  },
  {
    path: prefix_root + 'form',
    component: TemplateFormComponent,
    data: { title: 'form' }
  },
  {
    path: prefix_root + 'auth',
    component: AuthComponent,
    data: { title: 'comp2' }
  },
  // { path: '',
  //   redirectTo: '/heroes',
  //   pathMatch: 'full'
  // },
  // { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    // Comp1Component,
    Comp2Component,
    AuthComponent,
    TemplateFormComponent,
    LetDirective,
    SrtTimeInputComponent,
    HeaderComponent,

    // MatCheckboxModule,
    // MdbInputDirective,
    // JokeListComponent, JokeFormComponent, JokeComponent,
  ],
  imports: [
    MatSidenavModule,
    SocketIoModule.forRoot(config),
    NgbModule,
    HttpClientModule,
    ngfModule ,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatToolbarModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    BrowserModule,

    // MatButtonToggleModule,
    // MatAutocompleteModule,

    // MatCheckboxModule,
    // MatChipsModule,
    // MatDatepickerModule,
    // MatDialogModule,
    // MatExpansionModule,
    // MatGridListModule,
    MatIconModule,
    MatListModule,
    // MatMenuModule,
    // MatPaginatorModule,
    // MatProgressBarModule,
    // MatProgressSpinnerModule,
    // MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    // MatSliderModule,
    // MatSlideToggleModule,
    // MatSnackBarModule,
    // MatTableModule,
    MatTabsModule,
    // MatTooltipModule,
    // MatButtonModule,
    // MatCheckboxModule,
    // MatDialogModule,
    // MatRadioModule,
    // Ng2SmartTableModule,
    // BrowserAnimationsModule
  ],
  providers: [/*AuthService*/HttpClientModule],
  bootstrap: [AppComponent],
  exports: [
    MatTabsModule,
    MatSidenavModule
  ],
})
export class AppModule { }
