import {
    Component,
    ViewChild,
} from '@angular/core';
import {
    FormsModule,
    FormGroup,
    FormControl,
    FormBuilder,
} from '@angular/forms';
import {not_angular_root} from '../utils_ang';
import { Subscription } from 'rxjs'
import {control_form, getTranslation,control_form_arg} from '../../../../../common_angular_node/controlForm'
import { SocketService } from '../Services/socket.service';
import { Translation } from '../Services/translation.service';
import {langs} from '../templateForm/langs'
import {
  HttpClient, HttpRequest,
  HttpResponse, HttpEvent
} from '@angular/common/http'


// const toFormData = function(o){
//   const v = o.formGroup.value,
//       formData:FormData = o.formData || new FormData(),
//       exepts = o.exepts
//   const data:any = {}
//   const appendVal = function(v:any){
//      for (const key of Object.keys(v) ) {
//         if(exepts[key]) continue
//         const value = v[key];
//         data[key] = value
//     }
//   }
//   appendVal(v)
//   if(o.add) appendVal(o.add)
//   o.formData.append('data', JSON.stringify(data))
//   return formData;
// }
interface argsSrtTime {
  formGroup: FormGroup,
  formControlName: string,
  label: string
  error_message: any
}


@Component({
  selector: 'template-form',
  templateUrl: './templateForm.component.html',
  styles: [
`   

.nv-file-over { border: dotted 3px red; } /* Default class applied to drop zones on over */
    .another-file-over-class { border: dotted 3px green; }


  .mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{
    margin-top: 0
  }

  

  .mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper {
    margin-top: 0.0em;
    top: calc(100% - 1.66666667em);
  }



  .mat-form-field-wrapper,.mat-form-field,.mat-form-field-infix{
    padding: 0 !important;
    margin: 0 !important;
  }

  .mat-form-field-subscript-wrapper{
    margin-top: 0 !important;
  }
  .form-control:focus {
      color: #495057;
      background-color: #fff;
      border-color: #80bdff;
      outline: 0;
      box-shadow: initial
  }
  .btn_space{margin-right: 5px}
    .dropable_zone{padding:50px}
    .mat-card{padding: 0}
  	.demo-basic[_ngcontent-kkl-c7] { padding: 0; } 
	.mat-card-content { padding: 16px; }
	.demo-full-width { width: 100%; }
	.demo-card { margin: 16px; }
	.demo-card   mat-card-content { font-size: 16px; }
	.demo-text-align-end { text-align: end; }
	.demo-textarea { resize: none; border: none; overflow: auto; padding: 0; background: lightblue; }
		@keyframes cdk-text-field-autofill-color-0 { to { background: transparent; color: red; }
	}
	.demo-custom-autofill-style:-webkit-autofill { animation-name: cdk-text-field-autofill-color-0; animation-fill-mode: both; }
	.demo-custom-autofill-style.cdk-text-field-autofill-monitored:-webkit-autofill { animation-name: cdk-text-field-autofill-start, cdk-text-field-autofill-color-0; }
 .demo-rows { width: 30px; }
  .my-drop-zone {
            border: dotted 3px lightgray;
        }
        /* Default class applied to drop zones on over */
        
        .invalid-drag {
            border: dotted 3px red;
        }
        
        .valid-drag {
            border: dotted 3px green;
        }
        
        html,
        body {
            height: 100%;
        }
        
        .previewIcon {
            width: 100px;
            height: 100px;
            background-size: cover;
            background-repeat: no-repeat;
        }
        
        .inline-block {
            display: inline-block;
            margin: .2em;
        }

        .mat-form-field{
          width: 212px;
          /* height: 59px; */
          line-height: 37px;
        }

 `] 
})
class TemplateFormComponent {
    // store the form in this.f
    @ViewChild('f') form: any;
    // Contains all control and field values
    userForm: FormGroup
    maxSize = 100000
    langs = langs
    //shorcut to avoid this.userForm.get('name')
    f = {
      audio_time: new FormControl('00:00:00,000'/*, (fc: FormControl) => this.getError({
        check_only_this_field: 'audio_time',fc
      })*/),
      srt_time: new FormControl('00:00:00,000'/*, (fc: FormControl) => this.getError({
        check_only_this_field: 'audio_time', fc
      })*/),
      audio_time_2: new FormControl(''/*, (fc: FormControl) => this.getError({
        check_only_this_field: 'audio_time', fc
      })*/),
      srt_time_2: new FormControl(''/*, (fc: FormControl) => this.getError({
        check_only_this_field: 'audio_time', fc
      })*/),
      target_lang: new FormControl(''),
      source_lang: new FormControl(''),
      files: new FormControl(null/*, (fc: FormControl) => this.getError({
        check_only_this_field: 'files', fc
      })*/)// only for control
    }
    // only accept srt files
  	accept = 'srt'
    lang = ["zh","en","af","sq","am","ar","hy","az","ba","eu","be","bn","bs","bg","my","ca","ceb","zh","hr","cs","da","nl","sjn","emj","en","eo","et","fi","fr","gl","ka","de","el","gu","ht","he","mrj","hi","hu","is","id","ga","it","jv","ja","kn","kk","km","ko","ky","lo","la","lv","lt","lb","mk","mg","ms","ml","mt","mi","mr","mhr","mn","ne","no","pap","fa","pl","pt","pa","ro","ru","gd","sr","si","sk","sl","es","su","sw","sv","tl","tg","ta","tt","te","th","tr","udm","uk","ur","uz","vi","cy","xh","yi","en","zh","af","sq","am","ar","hy","az","ba","eu","be","bn","bs","bg","my","ca","ceb","zh","hr","cs","da","nl","sjn","emj","en","eo","et","fi","fr","gl","ka","de","el","gu","ht","he","mrj","hi","hu","is","id","ga","it","jv","ja","kn","kk","km","ko","ky","lo","la","lv","lt","lb","mk","mg","ms","ml","mt","mi","mr","mhr","mn","ne","no","pap","fa","pl","pt","pa","ro","ru","gd","sr","si","sk","sl","es","su","sw","sv","tl","tg","ta","tt","te","th","tr","udm","uk","ur","uz","vi","cy","xh","yi"]
    lang_label = ["Chinese","English","Afrikaans","Albanian","Amharic","Arabic","Armenian","Azerbaijani","Bashkir","Basque","Belarusian","Bengali","Bosnian","Bulgarian","Burmese","Catalan","Cebuano","Chinese","Croatian","Czech","Danish","Dutch","Elvish (Sindarin)>","Emoji","English","Esperanto","Estonian","Finnish","French","Galician","Georgian","German","Greek","Gujarati","Haitian","Hebrew","Hill Mari","Hindi","Hungarian","Icelandic","Indonesian","Irish","Italian","Javanese","Japanese","Kannada","Kazakh","Khmer","Korean","Kyrgyz","Lao","Latin","Latvian","Lithuanian","Luxembourgish","Macedonian","Malagasy","Malay","Malayalam","Maltese","Maori","Marathi","Mari","Mongolian","Nepali","Norwegian","Papiamento","Persian","Polish","Portuguese","Punjabi","Romanian","Russian","Scottish Gaelic","Serbian","Sinhalese","Slovak","Slovenian","Spanish","Sundanese","Swahili","Swedish","Tagalog","Tajik","Tamil","Tatar","Telugu","Thai","Turkish","Udmurt","Ukrainian","Urdu","Uzbek","Vietnamese","Welsh","Xhosa","Yiddish"]
    error_message = {}
    // do not wait focus out to display errors
    // errorMatcher = new CrossFieldErrorMatcher();

    //progress of the upload
    progress:number = 0
    progress_server = 0

    // url to upload srt
    url = not_angular_root + 'upload'

    // files that will be upload
    files: File[] = []

    // formdata that contains files to upload 
    // Populated via ngfFormData directive
    // he is filled with others attribute before
    // sending the request

    // TODO : remove sendableFormData and create it only to send upload from "this.files"
    sendableFormData:FormData = null

    // last output from server 
    server_download_files: any

    hasBaseDropZoneOver:boolean = false
    httpEmitter:Subscription
    httpEvent:HttpEvent<{}>
    lastFileAt:Date
    hasAnotherDropZoneOver:boolean = false;


    lastInvalids:File[] = []
    baseDropValid:boolean = true
    dragFiles:boolean = false
    fields_with_error = {}
    ajaxId: number = 1
    constructor(public HttpClient:HttpClient, private fb: FormBuilder, private socket: SocketService, private translation: Translation) {
      this.f.audio_time.hasError('required')
      this.ajaxId = +(localStorage.getItem('next_ajaxId') || 0)
      localStorage.setItem("next_ajaxId",'' + (this.ajaxId + 1) );
      this.socket.emitMessage()
      this.socket.on('progression_file_translation', (o:any) => {
        console.log(o)
        //if the answer if for this tab or if it is for all tab
        if(o.ajaxId == this.ajaxId || o.ajaxId === 0){
          console.log(o)
          this.progress_server = (1 - (o.not_yet_succeed / o.max)) * 100
        }
      })
      this.initForm();
    }

    getError({check_only_this_field}:{check_only_this_field:string, fc: FormControl}){
        // console.log('is called')
        
        // let array_of_error = control_form({
        //   audio_time: this.userForm.get('audio_time').value,
        //   srt_time: this.userForm.get('srt_time').value,
        //   audio_time_2: this.userForm.get('audio_time_2').value,
        //   srt_time_2: this.userForm.get('srt_time_2').value,
        //   check_only_this_field
        // })
        // let obj_error: any = {}
        // array_of_error.forEach(function(error: any){
        //   obj_error[error.error_message] = true
        //   error.fields.forEach(function(field: string){
        //       // if(!obj_error[field]){

        //       //     this.fields_with_error[field] = true
        //       // }
        //   })
        // })
        // console.log(obj_error)
        return null
    }


    argsSrtTime(formControlName:string, label:string):argsSrtTime{
      return {
          formGroup: this.userForm,
          formControlName,
          label,
          error_message: this.error_message,
      }
    }

    // TODO: Set form validation in a service
    initForm() {
        let nb_control_is_running = 0;
        this.userForm = this.fb.group(this.f, {
            //TODO: add error at the top of form  
            validator: (form: FormGroup) => {
                ++nb_control_is_running
                // console.log('is called')
                let fcont = {
                    audio_time: form.get('audio_time').value,
                    srt_time: form.get('srt_time').value,
                    audio_time_2: form.get('audio_time_2').value,
                    srt_time_2: form.get('srt_time_2').value,
                    target_lang: form.get('target_lang').value,
                    source_lang: form.get('source_lang').value
                }
                let array_of_error = control_form(<control_form_arg> fcont)
                //reset errors
                const all_fields = Object.keys(fcont)
                reset_cross_field_error : {
                    all_fields.forEach((field_s:string)=>{
                        let field = form.get(field_s)
                        // we have to reset error otherwise they will stay
                        if(field.status !== 'VALID' ){
                            // TODO automaticaly detect errors on 2 fields or more to remove them
                            delete field.errors['time_order']
                            delete field.errors['same_language']
                            delete field.errors['only_one_lang']
                            if(!Object.keys(field.errors).length) { // if no errors left
                                field.setErrors(null); // set control errors to null making it VALID
                            }else{
                                field.setErrors(field.errors); // controls got other errors so set them back
                            }
                        }
                    })
                }
                add_the_first_error : {
                    const has_already_error: Set<string> = new Set()
                    let obj_error: any = {}
                    array_of_error.forEach((error: any)=>{
                        error.fields.forEach((field: string)=>{
                            if(has_already_error.has(field)){
                                return;
                            }
                            has_already_error.add(field)
                            form.get(field).setErrors({[error.error_code]: true});
                            let f = this.error_message[field] = this.error_message[field] || {}
                            if(!f){
                              console.error('no message for this error')
                            }else{
                              f[error.error_code] = getTranslation(error.error_code, error.error_params)
                            }
                        })
                    })  
                }

                // run others controls
                if(nb_control_is_running == 0){
                  all_fields.forEach((field_s:string)=> {
                    form.get(field_s).updateValueAndValidity()
                  })
                }
                --nb_control_is_running
                // console.log(obj_error)
                return null
            }
      })
      console.log(this.userForm.hasError('toto'))
    }

    filesChange(){
      this.lastFileAt = this.getDate()
      // only usefull to do control because we will use Formdata to 
      // send data and not the formGroup
      // this.f.files.setValue(this.files)
      // console.log(this.sendableFormData)
    }

    getFirstErrorMessageLang(){
      return this.translation.getFirstErrorMessage({
        formControl: this.f.target_lang, 
        formControlName: 'target_lang',
        error_message: this.error_message
      })
    }

    cancel(){
        this.progress = 0
        if( this.httpEmitter ){
          console.log('cancelled')
          this.httpEmitter.unsubscribe()
        }
    }
   
    public fileOverBase(e:any):void {
      this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e:any):void {
      this.hasAnotherDropZoneOver = e;
    }

    uploadFiles(files:File[]):Subscription{
        const sendableFormData = new FormData()
        this.progress_server = 0
        const input_params = {...this.userForm.value}
        delete input_params.files
        // Put data before file is important, because we expect 
        // them to be load in less than 5s on the server
        sendableFormData.append('data', JSON.stringify({
          input_params,
          request_info: {
            ajaxId: this.ajaxId
          }
        }))
        for(let file of this.files){
          sendableFormData.append('file[]', file, file.name)
        }
        const req = new HttpRequest<FormData>('POST', this.url, sendableFormData, {
          reportProgress: true//, responseType: 'text'
        })
    
        return this.httpEmitter = this.HttpClient
            .request(req)
            .subscribe( (event: HttpResponse<any>) => {
                this.httpEvent = event
                if (event instanceof HttpResponse){
                    delete this.httpEmitter
                    console.log('request done', event)
                    if(!event.body.files){
                        console.error('no files')
                    }
                    this.server_download_files = event.body
                }
            },
              error=>console.log('Error Uploading',error)
            )
    }
 
    getDate(){
      return new Date()
    }

    // model: Signup = new Signup();

    // langs: string[] = [
    //   'English',
    //   'French',
    //   'German',
    // ];

  onSubmit() {
    if (this.form.valid) {
      console.log("Form Submitted!");
      this.form.reset();
    }
  }
}

export {TemplateFormComponent,FormsModule}
