import { Injectable } from '@angular/core';


let id = Math.random();

@Injectable({
  providedIn: 'root'
})
export class Translation {
	constructor() {		
		console.log(id)
	}

	getFirstErrorMessage({formControl, formControlName,error_message}:any):string | null{
		let translation = error_message[formControlName]
		// translation is an object with the error as key and the message as value
		// the presence of a translation does not mean that the error is present
		if(translation){
			for(let error_code of Object.keys(formControl.errors || {})){
				let error_m = translation[error_code]
				if(error_m){
					return error_m
				}
			}
		}
		return null
	}

}