import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
// import {patch_socket}  from './patch_socket.js'
// interface Socket {
//     $emit: Function;
//     on : Function;
//     emit: Function
// }


@Injectable({
  providedIn: 'root'
})
export class SocketService {
	constructor(private socket: Socket) {		
		// this.socket.on('progression_file_translation', function (data) {
		//     console.log(data);
		// });
	}

	emitMessage() {
		this.socket.emit('addDoc', { id: 4, doc: '' });
	}

	on(name,func){
		// this.socket.on('progression_file_translation', function (data) {
		//     console.log('rrrrr');
		// });
		return this.socket.on(name,(...args)=>{
			func(...args)
		})
	}

}