"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const auth_service_1 = require("../Services/auth.service");
let AuthComponent = class AuthComponent {
    constructor(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    ngOnInit() {
        this.authStatus = this.authService.isAuth;
    }
    onSignIn() {
        this.authService.signIn().then(() => {
            console.log('Sign in successful!');
            this.authStatus = this.authService.isAuth;
            this.router.navigate(['comp2']);
        });
    }
    onSignOut() {
        this.authService.signOut();
        this.authStatus = this.authService.isAuth;
    }
};
AuthComponent = __decorate([
    core_1.Component({
        selector: 'app-auth',
        templateUrl: './auth.component.html',
        styleUrls: ['./auth.component.scss'],
        providers: [auth_service_1.AuthService]
    })
], AuthComponent);
exports.AuthComponent = AuthComponent;
