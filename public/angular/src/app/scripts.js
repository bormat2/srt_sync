"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const forms_1 = require("@angular/forms");
exports.FormsModule = forms_1.FormsModule;
const utils_ang_1 = require("./utils_ang");
const http_1 = require("@angular/common/http");
class CrossFieldErrorMatcher {
    isErrorState(control) {
        if (control) {
            const hasInteraction = control.dirty || control.touched;
            const isInvalid = control.invalid;
            return !!(hasInteraction && isInvalid);
        }
        return false;
    }
}
const toFormData = function (o) {
    const v = o.formValue.value, formData = o.formData || new FormData(), exepts = o.exepts;
    for (const key of Object.keys(v)) {
        if (exepts[key])
            continue;
        const value = v[key];
        formData.append(key, value);
    }
    return formData;
};
let TemplateFormComponent = class TemplateFormComponent {
    constructor(HttpClient, fb) {
        this.HttpClient = HttpClient;
        this.fb = fb;
        this.maxSize = 100000;
        this.f = {
            audio_time: new forms_1.FormControl('00:00:00,000'),
            srt_time: new forms_1.FormControl('00:00:00,000'),
            audio_time_2: new forms_1.FormControl('00:00:00,000'),
            srt_time_2: new forms_1.FormControl('00:00:00,000'),
            files: new forms_1.FormControl(null)
        };
        this.accept = 'srt';
        this.errorMatcher = new CrossFieldErrorMatcher();
        this.url = utils_ang_1.not_angular_root + 'upload';
        this.files = [];
        this.hasBaseDropZoneOver = false;
        this.hasAnotherDropZoneOver = false;
        this.lastInvalids = [];
        this.f.audio_time.hasError('required');
        this.initForm();
    }
    initForm() {
        this.userForm = this.fb.group(this.f, {
            validator: (form) => {
                const condition = form.get('audio_time').value === form.get('srt_time').value;
                return condition ? { times_are_the_same: true } : null;
            }
        });
    }
    filesChange() {
        this.lastFileAt = this.getDate();
        this.f.files.setValue(this.files);
    }
    cancel() {
        this.progress = 0;
        if (this.httpEmitter) {
            console.log('cancelled');
            this.httpEmitter.unsubscribe();
        }
    }
    fileOverBase(e) {
        this.hasBaseDropZoneOver = e;
    }
    fileOverAnother(e) {
        this.hasAnotherDropZoneOver = e;
    }
    uploadFiles(files) {
        const req = new http_1.HttpRequest('POST', this.url, toFormData({
            formValue: this.userForm,
            formData: this.sendableFormData,
            exepts: { files: true }
        }), {
            reportProgress: true
        });
        return this.httpEmitter = this.HttpClient
            .request(req)
            .subscribe((event) => {
            this.httpEvent = event;
            if (event instanceof http_1.HttpResponse) {
                delete this.httpEmitter;
                console.log('request done', event);
                if (!event.body.files) {
                    console.error('no files');
                }
                this.server_download_files = event.body;
            }
        }, error => console.log('Error Uploading', error));
    }
    getDate() {
        return new Date();
    }
    onSubmit() {
        if (this.form.valid) {
            console.log("Form Submitted!");
            this.form.reset();
        }
    }
};
__decorate([
    core_1.ViewChild('f')
], TemplateFormComponent.prototype, "form", void 0);
TemplateFormComponent = __decorate([
    core_1.Component({
        selector: 'template-form',
        templateUrl: './templateForm/templateForm.component.html',
        styles: [
            `   

.nv-file-over { border: dotted 3px red; } /* Default class applied to drop zones on over */
    .another-file-over-class { border: dotted 3px green; }


  .mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{
    margin-top: 0
  }

  

  .mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper {
    margin-top: 0.0em;
    top: calc(100% - 1.66666667em);
  }



  .mat-form-field-wrapper,.mat-form-field,.mat-form-field-infix{
    padding: 0 !important;
    margin: 0 !important;
  }

  .mat-form-field-subscript-wrapper{
    margin-top: 0 !important;
  }
  .form-control:focus {
      color: #495057;
      background-color: #fff;
      border-color: #80bdff;
      outline: 0;
      box-shadow: initial
  }
  .btn_space{margin-right: 5px}
    .dropable_zone{padding:50px}
    .mat-card{padding: 0}
  	.demo-basic[_ngcontent-kkl-c7] { padding: 0; } 
	.mat-card-content { padding: 16px; }
	.demo-full-width { width: 100%; }
	.demo-card { margin: 16px; }
	.demo-card   mat-card-content { font-size: 16px; }
	.demo-text-align-end { text-align: end; }
	.demo-textarea { resize: none; border: none; overflow: auto; padding: 0; background: lightblue; }
		@keyframes cdk-text-field-autofill-color-0 { to { background: transparent; color: red; }
	}
	.demo-custom-autofill-style:-webkit-autofill { animation-name: cdk-text-field-autofill-color-0; animation-fill-mode: both; }
	.demo-custom-autofill-style.cdk-text-field-autofill-monitored:-webkit-autofill { animation-name: cdk-text-field-autofill-start, cdk-text-field-autofill-color-0; }
 .demo-rows { width: 30px; }
  .my-drop-zone {
            border: dotted 3px lightgray;
        }
        /* Default class applied to drop zones on over */
        
        .invalid-drag {
            border: dotted 3px red;
        }
        
        .valid-drag {
            border: dotted 3px green;
        }
        
        html,
        body {
            height: 100%;
        }
        
        .previewIcon {
            width: 100px;
            height: 100px;
            background-size: cover;
            background-repeat: no-repeat;
        }
        
        .inline-block {
            display: inline-block;
            margin: .2em;
        }


 `
        ]
    })
], TemplateFormComponent);
exports.TemplateFormComponent = TemplateFormComponent;
