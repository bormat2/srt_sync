"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const utils_ang_1 = require("./utils_ang");
let AppComponent = class AppComponent {
    constructor() {
        this.title = 'display-table';
        this.Object = Object;
        this.prefix_root = utils_ang_1.prefix_root;
        this.translation = {
            hour: 'hour',
            deal_id: 'deal id',
            refused: 'refused',
            accepted: 'accepted',
            client_id: 'client id',
            deal_name: 'deal name',
            client_name: 'client name'
        };
        this.settings = {
            actions: {
                add: false,
                delete: false,
                edit: false
            },
            columns: {}
        };
        this.col_names = Object.keys(this.settings.columns);
        this.data_csv = window.data_csv;
        this.source = this.data_csv.source;
        this.column_checked_at_loading = this.data_csv.column_checked_at_loading || ['client_id'];
        for (let col_name of this.column_checked_at_loading) {
            this.settings.columns[col_name] = { title: this.translation[col_name] };
        }
    }
};
AppComponent = __decorate([
    core_1.Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.scss']
    })
], AppComponent);
exports.AppComponent = AppComponent;
