"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SrtTimeInputComponent_1;
"use strict";
const core_1 = require("@angular/core");
const forms_1 = require("@angular/forms");
const ReplaySubject_1 = require("rxjs/ReplaySubject");
const core_2 = require("@angular/core");
let SrtTimeInputComponent = SrtTimeInputComponent_1 = class SrtTimeInputComponent {
    constructor() {
        this.delegatedMethodCalls = new ReplaySubject_1.ReplaySubject();
    }
    ngAfterContentInit() {
        this.formControl = this.formGroup.get(this.formControlName);
    }
    err(name) {
        return this.formControl.hasError(name);
    }
    ngAfterViewInit() {
        this.delegatedMethodCalls.subscribe(fn => fn(this.valueAccessor));
    }
    registerOnChange(fn) {
        this.delegatedMethodCalls.next(valueAccessor => valueAccessor.registerOnChange(fn));
    }
    registerOnTouched(fn) {
        this.delegatedMethodCalls.next(valueAccessor => valueAccessor.registerOnTouched(fn));
    }
    setDisabledState(isDisabled) {
        this.delegatedMethodCalls.next(valueAccessor => valueAccessor.setDisabledState(isDisabled));
    }
    writeValue(obj) {
        this.delegatedMethodCalls.next(valueAccessor => valueAccessor.writeValue(obj));
    }
};
__decorate([
    core_1.Input()
], SrtTimeInputComponent.prototype, "label", void 0);
__decorate([
    core_1.Input()
], SrtTimeInputComponent.prototype, "errorMatcher", void 0);
__decorate([
    core_1.Input()
], SrtTimeInputComponent.prototype, "required", void 0);
__decorate([
    core_1.Input()
], SrtTimeInputComponent.prototype, "formControlName", void 0);
__decorate([
    core_1.Input()
], SrtTimeInputComponent.prototype, "formGroup", void 0);
__decorate([
    core_1.ViewChild(forms_1.DefaultValueAccessor)
], SrtTimeInputComponent.prototype, "valueAccessor", void 0);
__decorate([
    core_2.ContentChild(forms_1.FormControlName)
], SrtTimeInputComponent.prototype, "formControl", void 0);
SrtTimeInputComponent = SrtTimeInputComponent_1 = __decorate([
    core_1.Component({
        selector: 'app-srt-time-input',
        templateUrl: './srt-time-input.component.html',
        styleUrls: ['./srt-time-input.component.scss'],
        providers: [{
                provide: forms_1.NG_VALUE_ACCESSOR,
                useExisting: core_1.forwardRef(() => SrtTimeInputComponent_1),
                multi: true
            }]
    })
], SrtTimeInputComponent);
exports.SrtTimeInputComponent = SrtTimeInputComponent;
