import { Component, Input, forwardRef, AfterViewInit, HostListener, OnChanges, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { NG_VALUE_ACCESSOR,FormControlName, ControlValueAccessor, FormControl, DefaultValueAccessor } from '@angular/forms';
import { ReplaySubject } from 'rxjs/ReplaySubject'
import {AfterContentInit, ContentChild, Directive} from '@angular/core';
import {ErrorStateMatcher} from '@angular/material';
import {
    NG_VALIDATORS,
    FormsModule,
    FormGroup,
    ValidatorFn,
    Validators,
    FormBuilder,
    FormGroupDirective,
    NgForm
} from '@angular/forms';

class CrossFieldErrorMatcher implements ErrorStateMatcher {
	constructor(private formGroup: FormGroup,private formControlName:String){}
    isErrorState(control: FormControl | null){
  	    if (control) {
	      const hasInteraction = control.dirty || control.touched;
	      const isInvalid = control.invalid;  
	      return !!(hasInteraction && isInvalid);
	    }

	    return false;
	}
}


interface argsSrtTime {
	formGroup: FormGroup,
	formControlName: string,
	label: string
	error_message: any
}

@Component({
	selector: 'app-srt-time-input',
	templateUrl: './srt-time-input.component.html',
	styleUrls: ['./srt-time-input.component.scss'],
	providers: [{
	    provide: NG_VALUE_ACCESSOR,
	    useExisting: forwardRef(() => SrtTimeInputComponent),
	    multi: true
	}]
})
export class SrtTimeInputComponent implements ControlValueAccessor, AfterViewInit {
	@Input() required:boolean;
	@Input() formGroup:FormGroup;
 	@Input() argsSrtTime:argsSrtTime;

	formControl: FormControl;
	errorMatcher: ErrorStateMatcher = null
	error_message: any
	formControlName:string
	label:string
	ngAfterContentInit(): void {
		const formGroup = this.argsSrtTime.formGroup
		this.formControlName = '' + this.argsSrtTime.formControlName
		this.label =  this.argsSrtTime.label
		this.formControl = <FormControl> formGroup.get('' + this.formControlName)
		this.errorMatcher = new CrossFieldErrorMatcher(formGroup,this.formControlName);
		this.error_message = this.argsSrtTime.error_message
	}

	onValueChanged(){
	}

	err(name:string){
		// console.log(this.formControl)
		return this.formControl.hasError(name)
	}

	getError(name:string){
		return this.error_message[this.formControlName][name]
	}

	getFirstErrorMessage():string | null{
		let translation = this.error_message[this.formControlName]
		// translation is an object with the error as key and the message as value
		// the presence of a translation does not mean that the error is present
		if(translation){
			for(let error_code of Object.keys(this.formControl.errors || {})){
				let error_m = translation[error_code]
				if(error_m){
					return error_m
				}
			}
		}
		return null
	}


	@ViewChild(DefaultValueAccessor) valueAccessor: DefaultValueAccessor;	
	delegatedMethodCalls = new ReplaySubject<(_: ControlValueAccessor) => void>();	

	ngAfterViewInit(): void {
		this.delegatedMethodCalls.subscribe(fn => fn(this.valueAccessor));
	}	

	registerOnChange(fn: (_: any) => void): void {
		this.delegatedMethodCalls.next(valueAccessor => valueAccessor.registerOnChange(fn));
	}
	registerOnTouched(fn: () => void): void {
		this.delegatedMethodCalls.next(valueAccessor => valueAccessor.registerOnTouched(fn));
	}	

	setDisabledState(isDisabled: boolean): void {
		this.delegatedMethodCalls.next(valueAccessor => valueAccessor.setDisabledState(isDisabled));
	}	

	writeValue(obj: any): void {
		this.delegatedMethodCalls.next(valueAccessor => valueAccessor.writeValue(obj));
	}
}