1
00:04:23,804 --> 00:04:25,848
- De l'huile ?
- De la poix, messire.

2
00:04:26,432 --> 00:04:27,475
Combien de barils ?

3
00:04:27,725 --> 00:04:28,476
Cinq cents.

4
00:04:31,145 --> 00:04:32,355
Trouves-en 500 de plus.

5
00:04:32,605 --> 00:04:33,522
Oui, messire.

6
00:04:46,327 --> 00:04:48,829
J'aime encore
qu'ils m'appellent messire.

7
00:04:49,080 --> 00:04:50,706
Ça finira par passer.

8
00:04:51,499 --> 00:04:52,917
Si on vit assez longtemps.

9
00:04:54,460 --> 00:04:56,087
Des hommes sans queue.

10
00:04:56,337 --> 00:04:57,630
Je me battrais pas

11
00:04:57,880 --> 00:04:59,465
si j'avais pas de queue.

12
00:04:59,966 --> 00:05:01,759
Pourquoi se battre ?

13
00:05:02,260 --> 00:05:03,553
Pour l'or.

14
00:05:04,262 --> 00:05:06,889
Je connais bien les soldats.
Ils le dépensent

15
00:05:07,139 --> 00:05:08,391
pour quoi ?

16
00:05:09,684 --> 00:05:10,977
Pour leur famille.

17
00:05:11,435 --> 00:05:12,937
On en a pas sans queue.

18
00:05:14,564 --> 00:05:17,400
Tout n'est qu'une question de queue,
au fond.

19
00:05:19,151 --> 00:05:19,902
Pourtant,

20
00:05:20,152 --> 00:05:22,863
votre frère s'est allié
avec des eunuques.

21
00:05:23,823 --> 00:05:26,659
Il a toujours défendu
les laissés-pour-compte.

22
00:06:06,282 --> 00:06:08,326
Et on va avoir notre compte.

23
00:06:43,444 --> 00:06:45,154
Combien de gens vivent ici ?

24
00:06:45,529 --> 00:06:46,781
Environ un million.

25
00:06:47,240 --> 00:06:48,741
Plus que dans tout le Nord.

26
00:06:49,283 --> 00:06:50,826
Entassés là.

27
00:06:51,118 --> 00:06:52,954
Pourquoi choisir de vivre ainsi ?

28
00:06:53,371 --> 00:06:55,248
Il y a plus de travail en ville.
