const translation:any = {
	'time_order': "The second {{name}} must be greater than the first",
	'required_cust': "The field {{field_label}} is required",
	'pattern_cust': "Please respect the pattern HH:mm:ss,FFF, of course, minutes \"mm\" and secondes \"ss\" must be less than 60",
	'same_language': "Translations with same source and target language are not possible, just let the language empty, if you don't want translation",
	'only_one_lang': "You have to choose the target and the source or none of them"
}
const getTranslation = (error_code:string, error_params: any):any=>{
	return translation[error_code].replace(/\{\{(.+?)\}\}/g,(match: string,$1:string)=>{
		return error_params[$1]
	})
}
interface error_info{
	error_code:string,
	error_params: any
	fields: string[]
}
interface control_form_arg{
	audio_time_2:string, 
	audio_time:string, 
	srt_time_2:string, 
	srt_time:string, 
	check_only_this_field:string,
	target_lang: string,
	source_lang: string,
	[propName: string]: string
}

const control_form = function(arg1: control_form_arg):error_info[]{

	const fields_with_error:Set<string> = new Set()
	const push_error = (err: error_info) => {
		errors.push(err)
		err.fields.forEach((name:string)=> fields_with_error.add(name))
	}
	const errors:error_info[] = []
	const check_only_this_field:string = arg1.check_only_this_field;
	const is_field = (name:string) => !check_only_this_field || check_only_this_field == name
	const add_error = (input1_name: string,input2_name: string, label1:string, label2:string)=>{
		const check_pattern = (input_name:string) => {
			if(! arg1[input_name].match(/^\d{2}:[0-5][0-9]:[0-5][0-9],\d{3}$/g )){
				push_error( <error_info> {
		    		error_code: 'pattern_cust',
		    		error_params: {name: input_name},
					fields: [input_name]
				})
				return true
			}
			return false
		}
		if(is_field(input1_name)){
			if(!arg1[input1_name]){
		    	push_error(<error_info> {
					error_code: 'required_cust',
					error_params: {field_label: label1},
					fields: [input1_name]
				})
		    }else{
		    	check_pattern(input1_name)
		    }
		}
		if(is_field(input2_name)){
			if(arg1[input2_name]){
				check_pattern(input2_name)
			}
		}
	}

	const add_error2 = function(input1_name: string,input2_name: string, label1:string, label2:string){
		if(is_field(input1_name) || is_field(input2_name)){
			// if there is no individual error on both field check if the order between field is good
			if(!fields_with_error.has(input1_name) || fields_with_error.has(input2_name)){
				if(arg1[input1_name] && arg1[input2_name] && arg1[input1_name] >= arg1[input2_name] ){
			    	push_error( <error_info> {
			    		error_code: 'time_order',
			    		error_params: {name: label1},
						fields: [input1_name, input2_name]
					})
				}
			}
	    }
	}
	add_error("audio_time","audio_time_2", 'audio_time', 'Audio time 2')
	add_error('srt_time','srt_time_2', 'Srt time', 'Srt time 2')
	add_error2("audio_time","audio_time_2", 'audio_time', 'Audio time 2')
	add_error2('srt_time','srt_time_2', 'Srt time', 'Srt time 2')

	// if language are same there is a probleme exept if the value is null
	if(arg1.target_lang && arg1.target_lang === arg1.source_lang){
		push_error( <error_info> {
    		error_code: 'same_language',
    		error_params: {},
			fields: ['target_lang', 'source_lang']
		})
	}

	// if there is only the target or only the source_lang
	if( ((+!!arg1.target_lang) ^ (+!!arg1.source_lang)) == 1){
		push_error( <error_info> {
    		error_code: 'only_one_lang',
    		error_params: {},
			fields: ['target_lang', 'source_lang']
		})
	}
    return errors
}

export {control_form, getTranslation,error_info,control_form_arg}