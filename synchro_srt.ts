(function(){
    const fs = require('fs-extra') // File System - for file manipulation
    const parser = require('subtitles-parser')
    const windows1252 = require('windows-1252')
    const {control_form} = require('./common_angular_node/controlForm')
    // const detectCharacterEncoding = require('detect-character-encoding');


    interface Synchro_srt_args{
        input_file_name: string;
        output_file_name: string;
        audio_time: string;
        srt_time: string;
        audio_time_2: string;
        srt_time_2: string
    }

    interface Get_corrector_func_args{
        not_linear: boolean;
        f_audio_time: number;
        f_srt_time: number;
        f_audio_time_2: number;
        f_srt_time_2: number;
    }

       // target_lang: string;
       //  source_lang:string
    class Srt_data{
        constructor(
            public startTime:string,
            public endTime:string,
            public text: string,
            public id: string//containing a number
        ){
            if(typeof id != 'string'){
                console.log(arguments)
                throw 'bad type index'
            }

        }
    }
    // const readFile = function(file){
    //     return new Promise(function(resolve){
    //         let val = fs.readFileSync(file)
    //         resolve(val)
    //     })
    // }

    const decodeBuffer = function(buffer: Buffer){
        const jschardet = require("jschardet")
        const str = buffer.toString('binary')
        if(!str){
            throw "empty string inside buffer";
        }
        console.log('jscharset', jschardet.detect(str))
        const jschardet_res = jschardet.detect(str)

        const is = (val:string) => jschardet_res === val
        if(is('windows-1252')){
            console.log('decode with windows-1252')
            return windows1252.decode(str)
        }
        return buffer.toString('UTF-8')
        // const charsetMatch = detectCharacterEncoding(buffer);
        // console.log('charsetMatch',charsetMatch)
    }
    const parse_srt_file = (file : string):Promise<Srt_data[]> => {
        const readFile = function():Promise<Srt_data[]>{
            return new Promise(function(resolve, reject){
                fs.readFile(file, (err : any, data: Buffer)=>{
                    if(err){
                        console.log('ERROR readfile parse srt file',err)
                        throw err
                    }else{
                        if(!data){
                            throw "ERROR: data buffer is empty";
                        }
                        let utf8_str: string = ''
                        try{
                            utf8_str = decodeBuffer(data)
                        }catch(e){
                            console.error('error has been catched',e)
                            reject(e.message)
                        }
                        if(!utf8_str){
                           throw 'ERROR: utf8_str is empty'
                        }
                        // console.error('ERROR: utf8_stre is : ',utf8_str.slice(0,100))
                        let array : Srt_data[] =  parser.fromSrt(utf8_str).map((o:any)=>{
                            // console.log(o)
                            return new Srt_data(
                                o.startTime,
                                o.endTime,
                                o.text,
                                o.id
                            )
                        })
                        if(array.length === 0){
                            console.error('ERROR 68:', utf8_str)
                            console.log(file)
                            throw 'ERROR:array is empty, there is no subtitle'
                        }
                        resolve(array)
                    }
                })
            })
        }

        const promise = readFile().catch((e)=>{
            console.error('Try again to read file')
            return readFile()
        })
        return promise
        // let array =  parser.fromSrt(fs.readFileSync(file,'utf8'))

    }


    // return milisecond:int of the string time
    const parseTime= (str:string) => {
        // console.log(str)
        const elems = str.replace(',',':').split(':')
        return  +elems[0] * 60 * 60 * 1000 + +elems[1] * 60 * 1000 + +elems[2] * 1000 + (+elems[3])
    }

    const stringifyTime = (t: number) => {
        if(t < 0){
            return '00:00:00,000'
        }
        // floor the number and add 0 before to have at least a certain length (only worked for length of 2 and 3)
        const min_digit = (number:number, min_digit:number)=>{
            let str_number = ''+ (number|0)
            return str_number.length > min_digit ? str_number : ('00000000' + str_number).slice(-min_digit)
        }
        const [h, m, s, ms] = [t / 3600000, t%3600000 / 60000, t%60000 / 1000, t%1000]
        return min_digit(h,2) + ':' + min_digit(m,2) + ':' + min_digit(s,2) + ',' + min_digit(ms,3)
    }

    // create the function that correct the time of subtitle
    // the function created worked with float and return a float,
    const get_corrector_func = ({not_linear, f_audio_time, f_srt_time,f_audio_time_2, f_srt_time_2}:Get_corrector_func_args) => {
        const first_srt_missing_time = f_audio_time - f_srt_time
        // if audio and subtitles have not the same speed
        // calculate the ratio to have the good speed
        const speed_ratio : number = !not_linear ? 1
            :(f_audio_time_2 - f_audio_time) / (f_srt_time_2 - f_srt_time)

        // note that if ratio is equal at 1 srt_time disapear of the function
        const start = first_srt_missing_time + f_srt_time
        return (time:number) => (start + (time - f_srt_time) * speed_ratio)
    }

    const check_srt_data = (data:Srt_data[])=>{
        data.forEach((data:Srt_data)=>{
            if(!data.id){
                throw 'the id does not exist'
            }
        })
    }
    const write_srt_file = (output_file_name: string,data:Srt_data[]) => {
        check_srt_data(data)
        const srt_txt = parser.toSrt(data)
        fs.writeFileSync(output_file_name, srt_txt, 'utf8')
    }

    const synchro_srt = ({input_file_name, output_file_name,
        audio_time, srt_time, audio_time_2,srt_time_2}:  Synchro_srt_args) => {

        let error = control_form({audio_time, srt_time, audio_time_2,srt_time_2})
        if(error.error_message){
            throw error.error_message
        }
        // correct_time accept a string time s_time
        // and return the repaired string time
        const correct_stime = (() => {
            const f_audio_time:number = parseTime(audio_time)
            const f_srt_time:number = parseTime(srt_time)
            const not_linear:boolean = !!(audio_time_2 && srt_time_2)
            const f_audio_time_2:number = +(not_linear && parseTime(audio_time_2))
            const f_srt_time_2:number = +(not_linear && parseTime(srt_time_2))
            let corrector = get_corrector_func({
                not_linear, f_audio_time, f_srt_time,f_audio_time_2, f_srt_time_2
            })
            return (s_time: string) => {
                if(!s_time){
                    throw 'no stime'
                }
                let a:number = parseTime(s_time)
                let b:number = corrector(a)
                let res:string = stringifyTime(b)
                return res
            }
        })()

        return parse_srt_file(input_file_name).then(function(data:any){
            // console.log(data)
            let ii2 = 0
            let data2 = <Srt_data[]> data
            for(let sub of data2){
                if(!sub.endTime || !sub.startTime){
                    console.log(sub)
                    throw 'no time : ' + ii2
                }
                sub.startTime = correct_stime(sub.startTime)
                sub.endTime = correct_stime(sub.endTime)
                ++ii2
            }
            if(!output_file_name){
                return data
            }
            write_srt_file(output_file_name, data)
            console.log()
        })
    }

    module.exports = {
        parse_srt_file,
        synchro_srt, // the only one you need others are exports for test
        write_srt_file,
        parseTime,
        stringifyTime,
        Srt_data
    }

})()
