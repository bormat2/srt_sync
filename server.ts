"use strict"
import {control_form,control_form_arg} from './common_angular_node/controlForm'
import express from 'express' // Express Web Server
import path from 'path' // used for file path
// import * as SocketDef from './socket.io'
import socketIO from './Socket.io/socket.io'
import * as https from 'https'
import * as http from 'http'

let prodMode = false; //updated later

// listening for uncaughtException must be done at soon as possible
process.on('uncaughtException', function (err) {
    console.log(err)
    // in dev mode stop node at the first error help to read errors 
    if(!prodMode){
        process.exit()
    }
})

interface CustomRequest extends Express.Request {
  busboy: any
  headers: any
  pipe: Function
  on: Function
}

interface RequestInfo{
    ajaxId: number
}
interface ReqInput{
    request_info:any
    input_params:any
}
interface MulterFile {
  key: string // Available using `S3`.
  path: string // Available using `DiskStorage`.
  mimetype: string
  originalname: string
  size: number
  pipe: Function
}

interface OptionAjax{
    options: any
    success: Function
    reject: Function
}
// interface Array<T> {
//     flat(deep: number): Array<T>;
//     [key:number]: T
// }

// interface Array_of_string<T> extends  Array<T>{
//     [key:number]: T
// }

// const SessionStore = require('sessionStore');
const session = require('express-session');
const {root_path} = require('./public/angular/src/gulp_info')
console.log('root_path', root_path)
const app = express()
const busboy = require('connect-busboy') // middleware for form/file upload
const fs = require('fs-extra') // File System - for file manipulation

const getUniqId = (() => {
    let last_uniq_id:string
    const uuidv4 = require('uuid/v4') // uniq name
    return function getUniqId(){
        let new_last_uniq_id = uuidv4()
        if(last_uniq_id === new_last_uniq_id){
            throw 'the unicity of id is not working'
        }
        return last_uniq_id =/*affect*/ new_last_uniq_id
    }
})()
// functions to synchronise subtitles and create a subtitle file
const {synchro_srt, write_srt_file,Srt_data} = require('./synchro_srt')

// create an url with some parameters
const querystring = require("querystring")

// zip a folder
const zipFolder = require('zip-folder');
// const https = require('https')
const args__44 = getArgs()
prodMode = args__44.prodMode
const {port} = args__44
//add line on console.log warn and error
defineDebugFunction(prodMode)
const {ajax_limited}  = get_ajax_limited()
init_server(port, root_path)

    // return; 

    /* **************************************************************************
     ***** After this comment code is not executed, there is only functions *****
     ****************************************************************************
    */

    function getArgs(){
        // the base url of the application
        // let root_path = '/'
        let prodMode = null
        let port = 3030
        // some parameter like --port 3030 have a value, new_arg_is_a_value is 
        // true if we expect the next argument to be a value
        let new_arg_is_a_value = false
        process.argv.forEach(function(val, i){
            if(!new_arg_is_a_value){
                switch (val) {
                    case "--port":
                        port = +process.argv[i+1]
                        new_arg_is_a_value = true
                        break;
                    // case "--root_path":
                    //     root_path = process.argv[i+1]
                    //     new_arg_is_a_value = true
                    //     break;
                    case '--prod':
                        prodMode = true
                        break;
                    case '--dev':
                        prodMode = false
                        break;
                    default:
                        break;
                }
            }else{
                new_arg_is_a_value = false
            }
        })

        if(prodMode === null){
            prodMode = false
            console_warn('Explicitly use --dev or --prod to avoid this warning prodMode has been set to --dev')
        }
        // root_path = ('/'+ root_path + '/').replace('\/*','/')
        console.log('root_path', root_path)
        return {prodMode, port, root_path}
    }

    function defineDebugFunction(prodMode:boolean){
        // Console.log warn and error will now displays the line of the error in 
        if(!prodMode){
            const cons:Console = console;
            console.log = get_cons('log', console.log)
            console.warn  = get_cons('warn', console.warn)
            console.error = get_cons('error', console.error)

            function get_cons(methodName:string, originalMethod: Function) {
                return (...args:any[]) => {
                    try {
                        throw new Error()
                    } catch (error) {
                        originalMethod.apply(cons,[error
                            .stack // Grabs the stack trace
                            .split('\n')[2] // Grabs third line
                            .trim() // Removes spaces
                            .substring(3) // Removes three first characters ("at ")
                            .replace(__dirname, '') // Removes script folder path
                            .replace(/\s\(./, ' at ') // Removes first parentheses and replaces it with " at "
                            .replace(/\)/, '') // Removes last parentheses
                            ,'\n',...args
                        ])
                    }
                }
            }
        }
    }

    function escapeRegExp(string: string) {
      return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
    }
     // defines routes accessibles with get method 
    function get_route_plus(opt: {url: string, func: Function}){
        const {url, func} = opt
        const listen_url = path.join(escapeRegExp(root_path), url)
        console.log('listen get : ', listen_url)
        return app.get(listen_url,<any> func)
    }

    // defines routes accessibles with get method 
    function get_route(url: string, ...args: any[]){
        const listen_url = path.join(root_path, url)
        console.log('listen get : ', listen_url)
        return app.get(listen_url, ...args)
    }

    // defines routes accessibles with post method 
    function post_route(url: string, ...args: any[]){
        const listen_url = path.join(root_path, url)
        console.log('listen post : ', listen_url)
        return app.route(listen_url).post( ...args)
    }

    // get a path inside the current folder 
    function rel_path(path_str: string){
        return path.join(__dirname, path_str)
    }

    function parseCookies(cookieStr:string) :any {
        return cookieStr.split(';').reduce((res, c) => {
            const [key, val] = c.trim().split('=').map(decodeURIComponent)
            const allNumbers = (str:string) => /^\d+$/.test(str);
            try {
              return Object.assign(res, { [key]: allNumbers(val) ?  val : JSON.parse(val) })
            } catch (e) {
              return Object.assign(res, { [key]: val })
            }
          }, {});
    }

    // 'io=t8Ta2Obl0XV8mRnRAAAA; ' +
    // 'connect.sid=s%3Ao_AhA1ck602RHpublpjxueZq18655BDE.1GBBnz19Vrz6qv%2F2tXMOUPlh9ZwTZfCMJlwnpV4RFYw'


    function ajax(options: Object){
        // console.log('ajax')
        return new Promise(function(resolve,reject){
            let i = 0
            let response_received = false
            const make_request = () => {
                // console.log('request created')
                // console.log(options)
                let req = https.request(options, function (res: http.IncomingMessage) {
                    response_received = true
                    if(res.statusCode !== 200){
                        // console.log(res)
                        console.log("statusCode: ", res.statusCode)
                        if(res.headers && res.headers.location){
                            console.log("location: ", res.headers.location)
                        }
                        reject('Request error ' + res.statusCode)
                    }
                    res.setEncoding('utf8')
                    let body = ''
                    res.on('data', function (data: any) {
                        // console.log('data_received')
                        body += data
                        if(!body){
                            throw 'there is no body'
                        }
                        // Too much POST data, kill the connection!
                        // 1e6 === 1MB
                        if (body.length > 1e6){
                            req.connection.destroy()
                        }
                    })

                    res.on('end', function() {
                        if(!body){
                            throw 'there is no body'
                        }
                        // console.log('YEEEEEEEEES')
                        // console.log(body)
                        setTimeout(()=>{
                            resolve(body)
                        },0)
                        
                    })
                })
                req.end()
                // if request does not respond in 2 s abort and try again
                setTimeout(()=>{
                    if(response_received) return
                    ++i
                    req.abort()
                    console.log('request aborded ' + i)
                    if(i < 10){
                        setTimeout(make_request,200)
                    }else{
                        reject('The server does not respond to this request even after 10 time')
                    }
                },10000)
            }

            make_request()
        })
    }

    // req.busboy.on('field') is trigger twice for the same
    // input, it is not normal so correct it.
    function on_field(req:any,func: Function){
        let set = new Set()
        req.busboy.on('field', function(...args: any[]) {
            let [fieldname] = args
            // if we dont have already used the property
            // call func
            if(!set.has(fieldname)){
                set.add(fieldname)
                func(...args)
            }
        })
    }

   

    function get_ajax_limited(){
         //do not create more than 10 simulaneous request
        let nb_ajax = 0
        let options_ajax:OptionAjax[] = []
        let checker_is_running = false
        const max_req = 15

        // check if we can launch request or we reach the limit and have to wait
        // the function is recursive while we don't have run all request
        const checker = function(){
            if(checker_is_running){
                return
            }
            checker_is_running = true
            const end = () => {
                --nb_ajax
                if(nb_ajax === 0){
                    checker_is_running = false
                    if(options_ajax.length){
                        _checker()
                    }
                }
            }  

            // allow to create max_req request, then wait they finnish and then continue 
            // with max_req until and repeat until there is no element in options_ajax 
            //
            // because if we send to much request at the same time the (other) server will not answer
            const _checker = function(){
                console.log('here')
                console.log('options_ajax.length', options_ajax.length)
                console.log('nb_ajax', nb_ajax)
                if(nb_ajax === 0){
                    while(options_ajax.length && nb_ajax < max_req){
                        // console.log('là')
                        ++nb_ajax
                        // console.log(nb_ajax)
                        let {options, success, reject}  = <OptionAjax> options_ajax.pop()
                        ajax(options).then((body) => {
                            // console.log('less worked')
                            success(body)
                            end()
                        }).catch((e)=>{
                            --nb_ajax
                            checker_is_running = false
                            console.log(e)
                            console.log('less because not worked')
                            reject(JSON.stringify(e))
                        })
                    }
                }else{
                    console.log('waiting end of others request')
                }
            }
            _checker()
        }

        const ajax_limited = function(options:http.RequestOptions):Promise<string>{
            return new Promise((resolve, reject) => {
                // console.log(options)
                options_ajax.push(<OptionAjax> {options, success: function(body:string){
                    // console.log('the body is ' , body)
                    resolve(body)
                }, reject})
                checker()
            })
        }
        return {ajax_limited}
    }

    // return a promise to execute code once the translation is done
    function translate_array(sentences_to_translate:string[], sess_id:string, io:any/*socketIO.server*/, ajaxId: number, target_lang:string, source_lang: string):Promise<string[]>{
        if(source_lang.length != 2 || source_lang.length != 2){
            throw 'source_lang and source_lang must be string of 2 characters'
        }
        return new Promise(function(resolve, reject){ 
            // we are limited by the number of character that we can translate
            // so separate in mutliple request
            // we can get the original array with the same length by using the flat function
            const split_array = function(max_character:number, sentences:string[]):string[][]{
                let length = 0
                let i = 0
                let arrays: string[][] = [[]]
                for(let sentence of sentences){
                    if(!sentence){
                        throw 'there is no sentence 1'
                    }
                    length += sentence.length
                    if(length > max_character){
                        length = 0
                        ++i
                        arrays[i] = []
                        if(!sentence){
                            throw 'there is no sentence 2'
                        }
                        // Maybe the sentence itself is too long and can't be translated
                        if(sentence.length > max_character){
                            console.log('this sentence was too long to be translated')
                            arrays[i].push('<too long>')
                            continue
                        }
                    }
                    arrays[i].push(sentence)
                }
                return arrays
            }
            //split to have max 1000 character by array of sentences
            if(sentences_to_translate.length === 0){
                throw 'empty sentences'
            }
            let array_of_array_of_sentence = split_array(1000,sentences_to_translate)
            console.log('first sentence', sentences_to_translate[0])
            console.log('length of array', array_of_array_of_sentence.length)
            if(!array_of_array_of_sentence){
                throw 'there is no array_of_array_of_sentence'
            }
            let not_yet_succeed = array_of_array_of_sentence.length
            let error = false
            let i = -1

            let array_of_array_of_sentence_translated:string[][]= []
            const lang = source_lang + '-' + target_lang
            console.error('lang',lang)
            if(!lang){
               console.log(source_lang,target_lang)
               throw 'lang is null'
            }
            for(let array_of_sentence of array_of_array_of_sentence){
                let i_bis = ++i
                if(array_of_sentence.length === 0){
                    throw 'empty array'
                }

                console.log('i_bis ? '+i_bis)
                const data = /*encodeURI(*/querystring.stringify({
                    key:'trnsl.1.1.20190429T214651Z.35c64a1dbec88f1b.2a7ba16c2d4fd6972631824cbe2ffd9e820a7496',
                    text: array_of_sentence,
                    lang
                })/*)*/

                

                // console.log('data to send', data)
                const options: http.RequestOptions = {
                    host: "translate.yandex.net",
                    port: 443,
                    path: "/api/v1.5/tr.json/translate?"+data,
                    method: "GET"
                }
                ajax_limited(options).then((body: string)=>{
                    console.log('first char'+ body.slice(0, 40));
                    // throw 'fake error'
                    if(!error){
                        // save sentences translated at the same index that original
                        // but in an other array 
                        --not_yet_succeed
                        let room = '<session>'+sess_id
                        console.log('req received ' + i_bis+'_'+sess_id)

                        io.sockets.in(room).emit('progression_file_translation',{
                            not_yet_succeed,
                            max: array_of_array_of_sentence.length,
                            ajaxId// id of the tab concerned
                        })
                        // console.log('not_yet_succeed', not_yet_succeed)
                        try{
                            // console.log('bodyyyy',body)
                            array_of_array_of_sentence_translated[i_bis] = JSON.parse(body).text
                            // array_of_array_of_sentence_translated.splice(i_bis,1,JSON.parse(body).text)
                        }catch(e){
                            console.log('json pb')
                            console.log('body')
                            throw e
                        }
                    }
                    // console.log('ajax succeed')
                    // console.log(body)

                    // when all requests have finished then call resolve
                    if(not_yet_succeed === 0){
                        if(!array_of_array_of_sentence_translated.flat){
                            console.log(array_of_array_of_sentence_translated)
                            let msg = "there is no flat function maybe you don't run node >=11"
                            console.log(msg)
                            throw msg
                        }
                        resolve(array_of_array_of_sentence_translated.flat(1))
                    }
                }).catch((error_reason)=>{
                    if(!error){
                        error = true
                        reject(error_reason)
                    }
                })
                // break
            }
        })
    }

    function createPromiseFileCorrected({
        fieldname, filename, path_folder, url_folder, file, all_fields_are_populated, json_response, res, get_io,sess_id
    }: {
       fieldname:string,
       filename:string,
       path_folder:string,
       url_folder:string,
       file: MulterFile,
       all_fields_are_populated: Promise<ReqInput>,
       json_response:any,
       res: any
       get_io: Function,
       sess_id: string
    }){
        return new Promise(function(resolve){
            // ++nb_files
            console.log(fieldname)
            // console.log('nb files ' + nb_files)
            console.log('Uploading: ' + filename)
            // Path where subtitles will be uploaded

            const file_full_name = filename + '.srt'
            const new_file_name = path_folder + '/' + file_full_name
            const input_relative_name = url_folder + '/' + file_full_name

            // p.input_file_name = new_file_name
            let output_file = fs.createWriteStream(new_file_name)
            file.pipe(output_file).on('finish', () => {
                console.log('finish')
                all_fields_are_populated.then(( oo:ReqInput)=>{
                    // now that file is moved and input ready we can correct our srt file
                    console.log('file ' + filename + 'will be synchronised')
                    const input_params:control_form_arg = oo.input_params
                    const request_info:RequestInfo = oo.request_info
                    const {source_lang,target_lang} = input_params
                    synchro_srt({
                        ...input_params, input_file_name: new_file_name
                    }).then((data_srt:any[]) =>{//todo replace any with the class Srt_data
                        if(data_srt && data_srt[0] && !(data_srt[0] instanceof Srt_data)){
                            throw 'bad type'
                        }
                        const sentences = data_srt.map((d:any)=> d.text)
                        // console.log('sentences repaired', sentences.slice(0,10))
                        if(sentences.length === 0){
                            throw 'empty sentences 1'
                        }
                        const end = () => {
                            json_response.files[filename] = input_relative_name
                            // --nb_file_process_running
                            resolve()
                        }
                        if(source_lang && target_lang){
                            console.log('source_lang',source_lang)
                            console.log('target_lang',target_lang)
                            translate_array(sentences,sess_id,get_io(), request_info.ajaxId,target_lang,source_lang).then((sentences_translated:string[])=>{
                                sentences_translated.forEach((sentence:string, i:number) => data_srt[i].text = sentence)
                                // Erase the old srt file with the translation
                                write_srt_file(new_file_name, data_srt)
                                end()
                                // json_response.files[filename] = input_relative_name
                                console.log('sentences translated for file '+ filename)
                                // --nb_file_process_running
                                // resolve()
                            }).catch((error_reason)=>{
                                res.writeHead(400,{Connection: 'close'})
                                res.end('An error has happened ' + error_reason)
                            })
                        }else{
                            end()
                        }
                    })
                })
            })  
        })     
    }
    // receive a request that contains subtitles and translate them 
    // and resynchronise them 
    function translate_and_synchro_srt(get_io:Function,req: CustomRequest, res:express.Response, next:any) {
        // const io = get_io()
        // const sess=req.session;
        const cookiesStr = req.headers.cookie || ''
        const cookies:any = parseCookies(cookiesStr);
        const sess_id = cookies['connect.sid']
        if(!sess_id){
            console.log(cookies)
            console.log(req.headers)
            throw 'The sess_id is not present'
        }
        const create_folder = ()=>{
            const folder_end = 'api/srt/' + getUniqId(),
                  path_folder = __dirname + '/public/' + folder_end,
                  url_folder ='./'+folder_end,
                  path_zip = path_folder + '.zip',
                  url_zip = url_folder + '.zip'
            
            // fs.mkdirSync(path_folder, 744);
            const folder_is_created = new Promise((resolve, reject)=>{
                fs.mkdir(path_folder, { recursive: false }, (err: any) => {
                  if (err){
                      reject()
                      throw err;
                  }
                  resolve()
                });
            })
            return {folder_is_created, path_folder, url_folder, path_zip, url_zip}
        }
        // create a new public folder and get the url and the path
        const {folder_is_created, path_folder, url_folder, path_zip, url_zip} = create_folder()
     
        // the response that we will send to the client
        const json_response = Object.preventExtensions({
            files: <any> {},
            zip: url_zip
        })

        //busboy provide helper function to get input parameters
        req.pipe(req.busboy)

        // prevent others properties to be added in p
        // TODO: check what happen if a user add a prop in the form
        // input parameters to repair srt
        // const not_received:string = ''
      
        const all_fields_are_populated:Promise<ReqInput> = new Promise((resolve, reject) => {
            // const input_params:control_form_arg = <control_form_arg> Object.preventExtensions({
            //     audio_time: not_received,
            //     srt_time: not_received,
            //     audio_time_2: not_received,
            //     srt_time_2: not_received,
            // })

            // const request_info = {
            //     ajaxId: 0
            // }

            // let nb_prop_not_loaded = Object.keys(input_params).length + Object.keys(request_info).length
            // let nb_prop_not_loaded = 1;
            let has_been_resolved = false;
            on_field(req, function(fieldname: string, val:any) {
                if(fieldname == 'data'){
                    const data:any = JSON.parse(val)
                    console.log('data',val)
                    const input_params:control_form_arg = <control_form_arg> data.input_params
                    const request_info:any = data.request_info
                    has_been_resolved = true
                    if(!input_params || !request_info || !request_info.ajaxId){
                        console.log('<'+JSON.stringify(data)+'>')
                        console.log(!input_params)
                        console.log(request_info)
                        throw 'missing parameters'
                    }
                    const array_of_error = control_form(<control_form_arg>input_params)
                    if(array_of_error.length){
                        console.log(array_of_error)
                        throw 'Some errors that should be handle client side have not be handled'
                    }
                    resolve({request_info, input_params} )
                }else{
                    throw 'forbiden prop ' + fieldname
                }
                // const obj_to_populate :control_form_arg | any= fieldname == 'ajaxId' ? request_info : input_params
                // obj_to_populate[fieldname] = val
                // console.log('nb_prop_not_loaded',nb_prop_not_loaded,fieldname, val)
                // if(--nb_prop_not_loaded === 0){
                //     // The object will no longer change
                //     Object.freeze(obj_to_populate)
                //     resolve({request_info, obj_to_populate} )
                // }
                // if(nb_prop_not_loaded < 0){
                //     throw "nb_prop_not_loaded must be positive or equal to 0"
                // }
            })
            setTimeout(()=>{
                if(!has_been_resolved){
                    const msg = 'not normal to be so long to load text fields, even empty text field has to be sent'
                    reject(msg)
                    // console.log('nb_prop_not_loaded',nb_prop_not_loaded)
                    // console.log('input_params',input_params)
                    console.log(msg)
                }
            },5000)
        })

        // For each srt files, translate and resynchronise 
        // but only once all field are loaded
        // let nb_files = 0 //TODO get the number directly from header
        // let nb_files_corrected = 0
        let promise_files_corrected:any[] = []
        // let nb_file_process_running = 0
        req.busboy.on('file', function(fieldname:string, file:MulterFile, filename:string) {
            console.log('busboy enter ' + filename)
            folder_is_created.then(function(){
                promise_files_corrected.push(createPromiseFileCorrected({
                    fieldname, filename, path_folder, url_folder, file, all_fields_are_populated, json_response, res, get_io, sess_id
                }))
            })
        })

        const total: number = +(req.headers['content-length'] || 0)
        let progress = 0

        //TODO: is it usefull ?
        // TODO protection against big file
        req.on('data', function(chunk:any){
            // chunk.log(chunk)
            if(!chunk){
                throw 'there is no chunk'
            }
            progress += chunk.length
            const perc: number = (progress/total)*100
            console.log('percent complete: '+perc+'%\n')
            // response.write('percent complete: '+perc+'%\n')
        })

        req.busboy.on('finish', function(){
            Promise.all(promise_files_corrected).then(function(){
                // if(nb_file_process_running !== 0){
                //     throw 'nb_file_process_running must be equal to 0'
                // }
                console.log('finish worked')
                res.writeHead(200,{Connection: 'close'})

                zipFolder(path_folder, path_zip, function(err: string) {
                    if(err) {
                        console.log('oh no!', err);
                    } else {
                        console.log('EXCELLENT');
                    }
                });
                console.log('json_response',json_response)
                res.end(JSON.stringify({...json_response, return_from: 234356/* just a random number to debug */}))
            })
        })
    }


    // defines all routes and start the server
    function init_server(port: number, root_path: string){
        const sessionStore = new session.MemoryStore();
        app.use(session({
            secret: 'ssshhrzzehhh',
            store: sessionStore
        }));

        app.use(busboy()) // decode form data
            // app.use(reg, express.static(rel_path( '/public/angular/dist/display-table/'+name)))

        ;['runtime(.*)','polyfills(.*)','main(.*)','styles(.*)','vendor(.*)','socket.io(.*)'].forEach((reg:string)=>{
            console.log('listen '+ root_path + reg) 
            // let reg2 = reg+'\\.map'
            get_route_plus({
                url : reg, 
                func: function(req:express.Request, res:express.Response){
                    console.log('the path is' + req.path)
                    const relative = req.path.replace(root_path,'')
                    res.sendFile(dist_main(relative));
                }
            })
            // get_route_plus({
            //     url : reg2, 
            //     func: function(req:express.Request, res:express.Response){
            //         console.log('the path is' + req.path)
            //     }
            // })
            // app.use(reg, express.static(rel_path( '/public/angular/dist/display-table/'+name)))
            // app.use(reg2, express.static(rel_path( '/public/angular/dist/display-table/' + name+".map")))

       })

        function dist_main(url: string){
            return rel_path( 'public/angular/dist/') + url
        }

         //folder where ressources are accessible
        app.use(root_path + 'assets', express.static(dist_main('assets')))
        app.use(root_path + 'api', express.static(rel_path( 'public/api')))
        // app.use(root_path + 'angular', express.static(dist_main('')))
       


        if(prodMode){
            get_route('/*',function(req:express.Request, res:express.Response){
                console.log('racine')
                res.render(dist_main('index.ejs'),{
                    // full_url : root_path,
                    root_path
                });
            })
            app.use(root_path, express.static(dist_main('')))
        }else{
            console_warn('the route / is not listen in devmode because you have to use npm run start_front to run client_side')
        }
        
        const get_io = ():any/*socketIO.server*/ => io
        post_route('/api/upload', translate_and_synchro_srt.bind(null,get_io))

        get_route('*', function(req:express.Request, res:express.Response) {
            res.end('get *'+req.url)
        })

        post_route('*', function(req:express.Request, res:express.Response) {
            res.end('post *'+req.url)
        })
       

        const server:any = app.listen(port, function() {
            console.log('Listening on port %d', server.address().port)
        })

        const io: any/*socketIO.server*/ = socketIO.listen(server)

        io.on('connection', function(socket: any){
            if(!socket.handshake.headers.cookie){
                console.log('no cookie, no enter')
                return;
            }
            console.log('an user connected')
            const cookiesStr = socket.handshake.headers.cookie;
            const cookies = parseCookies(cookiesStr);
            const sess_id = cookies['connect.sid']
            socket.emit('progression_file_translation',{test:5})
            // for security reason use a prefix, in case the sess_id is a fake
            const room = '<session>' + sess_id
            console.log('room', room)
            socket.join(room)
            socket.to(room).emit('request_start', {titi:'new socket for the user'})
        })

        // TODO clean sessions witch are closed
        // const sockets_by_session_id = {}
        //Auth the user
        io.set('authorization', function (data: any, accept:any) {
          // check if there's a cookie header
            if(data.headers.cookie) {
                const cookies = parseCookies(data.headers.cookie);
                if(!data.headers.cookie || !cookies['connect.sid']){
                    //reject the socket if there is no cookie
                    console.log('reject the connection')
                    return accept(false, 'try again when you will have session id')
                }
                //autorise the socket
                return accept(null, true)
                // if()
                // sessionStore.get(data.sessionID, function(err:any, session:any){
                //     if(err) throw err;
                //     if(!session){
                //         console.error("Error whilst authorizing websocket handshake");
                //         accept('Error', false);
                //     }else{
                //         console.log("AUTH USERNAME: " + session.username);
                //         if(session.username){
                //             data.session = {data, session}
                //             accept(null, true);
                //         }else {
                //             accept('Invalid User', false);
                //         }
                //     }
                // })
            } else {
                console.error("No cookie was found whilst authorizing websocket handshake");
                return accept('No cookie transmitted.', false);
            }
        });         
    }

    function console_warn(stringToMakeYellow:string){
        console.log('\x1b[33m'+ stringToMakeYellow +'\x1b[0m');  //yellow
    }
