module.exports = {
    env: {
        node: true,
        es6: true
    },
    parser: 'babel-eslint',
    parserOptions: {
        ecmaVersion: 7,
        ecmaFeatures: {
            experimentalObjectRestSpread: true
        },
        sourceType: 'module'
    },
    // extends: ['eslint:recommended'],
    rules: {
        semi: ['error', 'never'],
        curly: 0,
        'comma-dangle': [2, 'never'],
        'comma-spacing': 0,
        eqeqeq: [2, 'allow-null'],
        'key-spacing': 0,
        'no-mixed-spaces-and-tabs': 0,
        'no-underscore-dangle': 0,
        'no-shadow': 0,
        'no-shadow-restrincted-names': 0,
        'no-extend-native': 0,
        'no-var': 2,
        'new-cap': 0,
        'semi-spacing': 0,
        'space-unary-ops': 0,
        'space-infix-ops': 0,
        'consistent-return': 0,
        strict: 0,
        'no-console': 'off',
        indent: ['error', 4, {SwitchCase: 2}],
        'linebreak-style': ['error', 'unix']
    }
};
