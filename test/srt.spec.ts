//mocha
const {parse_srt_file, synchro_srt,parseTime,stringifyTime} = require('../synchro_srt')

const assert = require('assert')
const parseSRT = require('parse-srt')
const fs = require('fs')
const sub = require('subtitle')
// Subtitle.parse
// Subtitle.stringify
// Subtitle.stringifyVtt
// Subtitle.resync
// Subtitle.toMS
// Subtitle.toSrtTime
// Subtitle.toVttTime
let file_content = fs.readFileSync( __dirname + '/short.srt','utf8')
// console.log(file_content)
console.log(sub.parse(file_content))

const eq = function(a,b){
    assert.equal(a,b)
}

describe('SRT', function() {
    describe( 'repair srt', function(){
        let name = __dirname + '/Game of Thrones - 1x01-02 - Winter is Coming.HDTV.en.srt'
        it( 'parse_srt_file', function(){
            const data = parse_srt_file(name).then(()=>{
                eq(data[0].startTime, '00:01:55,418')
                eq(data[data.length - 1].startTime, '01:00:34,899')
            })
        })

        it('parse', function(){
            eq(parseTime('00:01:55,418'), 418 + 55 * 1000 + 1 * 1000 * 60)
        })

        it('stringify and parse', function(){
            eq(stringifyTime(parseTime('00:01:55,418')), '00:01:55,418')
        })



        it('complex shift of 5s and 10s', function(){
            const name_rep = __dirname + '/repaired.srt'
            //subtitles are late of 5s at the start but 10 at the end
            synchro_srt({
                input_file_name: name,
                output_file_name: name_rep,
                audio_time: '00:02:00,418',
                srt_time: '00:01:55,418',
                audio_time_2: '01:00:44,899',
                srt_time_2: '01:00:34,899'
            }).then(()=>{
                const data_repaired = parse_srt_file(name_rep).then((data_repaired)=>{
                    // now we should have the same time for srt that for audio
                    eq(data_repaired[0].startTime, '00:02:00,418')
                    eq(data_repaired[data_repaired.length - 1].startTime, '01:00:44,899')
                })
            })

            
           
        })


        it('simple shift of 5s', function(){
            const name_rep = __dirname + '/repaired.srt'
            //subtitle are late of 5s
            synchro_srt({
                input_file_name: name,
                output_file_name: name_rep,
                audio_time: '00:00:05,00',
                srt_time: '00:00:00,00'
            }).then(()=>{
                const data_repaired = parse_srt_file(name_rep)
                eq(data_repaired[0].startTime, '00:02:00,418')
                eq(data_repaired[data_repaired.length - 1].startTime, '01:00:39,899')
            })
        })


        // if the subtitle start before 0 start must be 0
        it('simple shift of 50s negative', function(){
            const name_rep = __dirname + '/repaired.srt'
            //subtitle are late of 5s
            synchro_srt({
                input_file_name: name,
                output_file_name: name_rep,
                audio_time: '00:00:00,00',
                srt_time: '00:50:00,00'
            }).then(function(){
                parse_srt_file(name_rep).then((data_repaired)=>{
                    eq(data_repaired[0].startTime, '00:00:00,000')
                    eq(data_repaired[data_repaired.length - 1].startTime, '00:10:34,899')
                }).catch((e)=>{
                    console.log(e)
                })
            })
        })
    })

})

