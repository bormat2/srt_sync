# [Srt_trans](https://gitlab.com/bormat2/srt_sync/edit/master/readme.md) &middot; [![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](#)

This application allows to resynchronise and translate caption with srt format.

If you choose to translate, you have to wait a little before the link to download appears, it depends of the speed of the Yandex API.

Demo : http://193.148.68.62:5555/random_path/form
## Installation

### Production mode:
When you build your application for production (nothing to do for dev mode) you have to know if the application will be at the root '/' of your server or in a folder, by default the folder is '/random_path/' You can change it in "public/angular/gulpfile.js" with the root_path variable

build client side :
	`npm run build_client`

run the typescript server :
`npm run watch-prod`

Go to http://localhost:5555/random_path/ (if you have choose '/random_path/' in "public/angular/gulpfile.json)

### Development mode:
run the client side :
	`npm run watch-dev`

run the typescript server :
	`npm run start_front`

In dev mode the node backend server and the frontend angular server are on 2 different port
a proxy is configured to avoid Cross problems.

Go to http://localhost:5200 (the server is on port 5555 to answer to ajax request only)

### License

Srt_sync is [MIT licensed](./LICENSE).


## Technologies used

* Node.js : 
    * Express for routing and render templates
    * busboy library to handle Files upload 
    * socket.io: to update the progress bar

* Angular 7 : 
    * Reactive Form with Material UI AND TypeScript / SCSS / 
    * Angular Routing 

* Gulp 4

* CSS: Bootstrap 4 and Boostrap glyphicons

* Use of translate.yandex.net API to translates sentences of subtitles

* Simple mocha script to test the resynchronysed function 
		`npm run test`




